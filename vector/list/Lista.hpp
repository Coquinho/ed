// Copyright 2016 Lucas Cavalcante de Sousa
template<typename T>
//! Implementação estrutural da lista
/*! Estrutura que se comporta como uma lista de objetos
 *  \author  Lucas Cavalcante de Sousa
 *  \since  8/4/16
 *  \version 1.0
 */
class Lista {
 private:
    T*l;  //!< inicio da lista
    int ultimo = -1;  //!< guarda o indice do ultimo elemento da lsita
                     //< se nao houver = -1
    int max = 10;  //!< tamanho maximo padrao da lista

 public:
    //! Construror da lista com tamanho maximo padrao(10)
    Lista() {
        l = new T[max];
    }
//! Construror da lista com tamanho tam
/*! 
 *  \param tam tamanhao desejado da lista
 */
    explicit Lista(int tam) {
        max = tam;
        l = new T[max];
    }
//! Adiciona dado ao final da lista
/*! Se a lista não estiver cheia adiciona um elemento ao fim
    caso contrario retorna um erro
 *  \param dado elemento a ser colocado ao fim da fila, tipo T
 *  \return void
 */
    void adiciona(T dado) {
         adicionaNaPosicao(dado, ultimo + 1);
    }
//! Adiciona dado ao inicio da lista
/*! Se a lista não estiver cheia adiciona um elemento ao inicio
    caso contrario retorna um erro
 *  \param dado elemento a ser colocado no inicio da fila, tipo T
 *  \return void
 */
    void adicionaNoInicio(T dado) {
        adicionaNaPosicao(dado, 0);
    }
//! Adiciona dado na posicao desejada da lista
/*! Se a lista não estiver cheia adiciona um elemento a posicao desejada
    caso contrario retorna um erro
 *  \param dado elemento a ser colocado na fila, tipo T
 *  \param posicao posicao a ser colocado o dado, tipo int
 *  \return void
 */
    void adicionaNaPosicao(T dado, int posicao) {
        if (listaCheia())
            throw "Lista Cheia";
        else if ((posicao > max ) | (posicao < 0))
            throw "Posicao Invalida";
        else if (posicao > ultimo + 1)
            throw "Posicao alta";
        for (int i = ultimo; i >= posicao; i--)
            l[i+1] = l[i];
        ultimo++;
        l[posicao] = dado;
    }
//! Adiciona dado em ordem
/*! Se a lista não estiver cheia adiciona um elemento em ordem
    caso contrario retorna um erro
 *  \param dado elemento a ser colocado na lista, tipo T
 *  \return void
 */
    void adicionaEmOrdem(T dado) {
        int i =0;
        while ((i <= ultimo) & (dado > l[i]))
            i++;
        adicionaNaPosicao(dado, i);
    }
//! Remove dado do final da lista
/*! Se a lista não estiver vazia remove o elemento ao fim
    caso contrario retorna um erro
 *  \return T
 */
    T retira() {
        return retiraDaPosicao(ultimo);
    }
//! Remove dado do inicio da lista
/*! Se a lista não estiver vazia remove o elemento do incio
    caso contrario retorna um erro
 *  \return T
 */
    T retiraDoInicio() {
        return retiraDaPosicao(0);
    }
//! Remove dado da posicao
/*! Se a lista não estiver vazia retira o elemento da posicao desejada
    caso contrario retorna um erro
 *  \param posicao posicao do elemento a ser retirado da lista, tipo int
 *  \return T
 */
    T retiraDaPosicao(int posicao) {
        if (listaVazia())
            throw "Lista Vazia";
        else if ((posicao > ultimo) | (posicao < 0))
            throw "Posicao Invalida";
        T aux = l[posicao];
        for (int i = posicao; i < ultimo; i++)
            l[i] = l[i+1];
        ultimo--;
        return aux;
    }
//! Remove dado
/*! Se a lista não estiver vazia adiciona um elemento em ordem
    caso contrario retorna um erro
 *  \param dado a ser removido da lista, tipo T
 *  \return T
 */
    T retiraEspecifico(T dado) {
        if (listaVazia())
            throw "Lista Vazia";
        return retiraDaPosicao(posicao(dado));

        throw "Dado não encontrado";
    }
//! Verifica posicao de um elemento
/*! Se a lista não estiver vazia verifica se o elemento desejado se encontra
    caso contrario retorna um erro
 *  \param dado elemento a ser verificado na lista, tipo T
 *  \return int , posicao
 */
    int posicao(T dado) {
        if (listaVazia())
            throw "Lista Vazia";
        for (int i = 0; i <= ultimo; i++)
            if (l[i] == dado)
                return i;
        throw "Dado não encontrado";
    }
//! Verifica se o elemento e contido na lista
/*! Se a o elemento estiver presente retorna true
    caso contrario retorna falso
 *  \param dado elemento a ser buscado na lista, tipo T
 *  \return bool
 */
    bool contem(T dado) {
        for (int i = 0; i <= ultimo; i++)
            if (l[i] == dado)
                return true;
        return false;
    }
//! Verifica igualdade
/*! Se a os dados forem iguais retorna true
    caso contrario retorna false
 *  \param dado1 elemento a ser verificado igualdade, tipo T
 *  \param dado2 elemento a ser verificado igualdade, tipo T
 *  \return bool
 */
    bool igual(T dado1, T dado2) {
        return (dado1 == dado2);
    }
//! Verifica maior
/*! Se a o dado1 for maior que o dado2 retorna true
    caso contrario retorna false
 *  \param dado1 elemento a ser verificado, tipo T
 *  \param dado2 elemento a ser verificado, tipo T
 *  \return bool
 */
    bool maior(T dado1, T dado2) {
        return (dado1 > dado2);
    }
//! Verifica menor
/*! Se a o dado1 for menor que o dado2 retorna true
    caso contrario retorna false
 *  \param dado1 elemento a ser verificado, tipo T
 *  \param dado2 elemento a ser verificado, tipo T
 *  \return bool
 */
    bool menor(T dado1, T dado2) {
        return (dado1 < dado2);
    }
//! Verifica se a lista esta cheia
/*! Se a a lista esta cheia retorna true
    caso contrario retorna false
 *  \return bool
 */
    bool listaCheia() {
        return ((max - 1) == ultimo);
    }
//! Verifica se a lista esta vazia
/*! Se a a lista esta vazia retorna true
    caso contrario retorna false
 *  \return bool
 */
    bool listaVazia() {
        return (ultimo == -1);
    }
//! Limpa a lista
/*! altera o indice do ultimo elemento para -1
 *  \return void
 */
    void destroiLista(){
        ultimo = -1;
    }
};

