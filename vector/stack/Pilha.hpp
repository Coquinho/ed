// Copyright 2016 Lucas Cavalcante de Sousa
template<typename T>
//! Implementação estrutural da pilha
/*! Estrutura que se comporta como uma pilha de objetos
 *  \author  Lucas Cavalcante de Sousa
 *  \since  1/4/16
 *  \version 1.0
 */
class Pilha {
 private:
	int ultimo = -1;  //!< guarda o indice do ultimo elemento da pilha
	                //< se nao houver = -1
	int max = 10;  //!< tamanho maximo padrao da pilha
	T*p;  //!< inicio da pilha

 public:
    //! Construror da pilha com tamanho maximo padrao(10)
    Pilha() {
        p = new T[max];
    }
    //! Construror da pilha com tamanho t
    /*! 
     *  \param t tamanhao desejado da pilha
     */
    Pilha<T>(int t) {
        p = new T[t];
    }
    //! Empilha dado à pilha
    /*! se a pilha não estiver cheia, adiciona um elemento ao fim
        caso contrario retorna um erro
     *  \param dado dado a ser empilhado
     *  \return void
     */
	void empilha(T dado) {
	    if (PilhaCheia())
	        throw "Pilha Cheia";
	    else
	        p[++ultimo] = dado;
	}
	//! desempilha dado da pilha
    /*! se a pilha não estiver vazia, retira e retorna o último elemento da pilha
        caso contrario retorna um erro
     *  \return um T
     */
	T desempilha() {
	    if (PilhaVazia())
	        throw "Pilha Vazia";
	    else
	        return p[ultimo--];
	}
	//! retorna o ùltimo elemento da pilha
    /*! se a pilha não estiver vazia, retorna o último elemento da pilha
        caso contrario retorna um erro
     *  \return um T
     */
	T topo() {
	    if (PilhaVazia())
	        throw "Pilha Vazia";
	    else
	        return p[ultimo];
	}
	//! retorna o indice do ultimo elemento da pilha
    /*! se a pilha não estiver vazia, retorna o indice do último elemento da pilha
        caso contrario retorna um erro
     *  \return um inteiro
     */
	int getPosTopo() {
	    if (PilhaVazia())
	        throw "Pilha Vazia";
	    else
	        return ultimo;
	}
	//! limpa a pilha
    /*! altera o indice do último elemento para -1
     *  \return void
     */
	void limparPilha() {
	    ultimo = -1;
	}
	//! Verifica se a pilha está vazia
    /*! se a pilha estiver vazia, retorna true
        caso contrario retorna false
     *  \return boolean
     */
	bool PilhaVazia() {
	    return (ultimo == -1);
	}
	//! Verifica se a pilha está cheia
    /*! se a pilha estiver cheia, retorna true
        caso contrario retorna false
     *  \return boolean
     */
	bool PilhaCheia() {
	    return (ultimo == (max - 1));
	}
};
