// Copyright 2016 Lucas Cavalcante de Sousa
template <typename T>
//! Implementação estrutural da fila
/*! Estrutura que se comporta como uma fila de objetos
 *  \author  Lucas Cavalcante de Sousa
 *  \since  1/4/16
 *  \version 1.0
 */
class Fila {
 private:
    int ult = -1;  //!< guarda o indice do ultimo elemento da fila
	             //< se nao houver = -1
    int max = 10;  //!< tamanho maximo padrao da pilha
    T*f;  //!< inicio da pilha


 public:
    //! Construror da fila com tamanho maximo padrao(10)
	Fila() {
	    f = new T[max];
	}
	//! Construror da fila com tamanho t
    /*!
     *  \param t tamanhao desejado da fila
     */
	Fila<T>(int tam) {
	    f = new T[tam];
    }
    //! inclui dado à fila
    /*! se a fila não estiver cheia, adiciona um elemento ao fim
        caso contrario retorna um erro
     *  \param dado dado a ser adicionado à fila
     *  \return void
     */
	void inclui(T dado) {
	    if (filaCheia())
	        throw "Fila Cheia";
	    else
	        f[++ult] = dado;
	}
    //! retira dado da fila
    /*! se a fila não estiver vazia, retira e retorna o primeiro elemento da fila
        e realoca o restante dos elementos,
        caso contrario retorna um erro
     *  \return um T
     */
	T retira() {
	    if (filaVazia()) {
	        throw "Fila Vazia";
	    } else {
	        T aux = f[0];
	        for (int i = 0; i < ult; i++)
	            f[i] = f[i+1];
	        ult--;
	        return aux;
	    }
	}
	//! retorna o ùltimo elemento da fila
        /*! se a pilha não estiver vazia, retorna o último elemento da filha
            caso contrario retorna um erro
         *  \return um T
         */
	T ultimo() {
	    if (filaVazia())
	        throw "Fila Vazia";
	    else
	        return f[ult];
	}
	//! retorna o indice do ultimo elemento da fila
    /*! se a pilha não estiver vazia, retorna o indice do último elemento da pilha
        caso contrario retorna um erro
     *  \return um inteiro
     */
	int getUltimo() {
	    if (filaVazia())
	        throw "Fila Vazia";
	    else
	        return ult;
	}
	//! Verifica se a fila está cheia
    /*! se a fila estiver cheia, retorna true
        caso contrario retorna false
     *  \return boolean
     */
	bool filaCheia() {
	    return (ult == (max - 1));
	}
	//! Verifica se a fila está vazia
    /*! se a fila estiver vazia, retorna true
        caso contrario retorna false
     *  \return boolean
     */
	bool filaVazia() {
	    return (ult == -1);
	}
	//! limpa a fila
    /*! altera o indice do último elemento para -1
     *  \return void
     */
	void inicializaFila() {
	    ult =-1;
	}
};

