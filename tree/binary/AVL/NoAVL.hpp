// Copyright 2016 Lucas Cavalcante de Sousa
//! Implementação estrutural da arvore binarria AVL
/*! Estrutura que se comporta como uma arvore de objetos
 *  \author  Lucas Cavalcante de Sousa
 *  \since  1/4/16
 *  \version 1.0
 */
#ifndef NO_AVL_HPP
#define NO_AVL_HPP
#include <vector>
#include <algorithm>

#define getInfo getDado
#define getRight getDireita
#define getLeft getEsquerda

template <typename T>
class NoAVL  {
 private:
    int height;  //!< Representa a altura do nó AVL
    T* info;  //!< chave do nodo
    NoAVL<T>* right;  //!< filho a direita do nodo
    NoAVL<T>* left;  //!< filho a esquerda do nodo
    std::vector<NoAVL<T>* > order;  //!< vetor com elementos em ordem

 public:
    //! Construtor Padão
    /*!
     * \param const T& other base para ciração da arvore
     */
    explicit NoAVL(const T& other) {
        info = new T{other};
        height = 0;
        left = nullptr;
        right = nullptr;
        order.clear();
    }

    //! Destrutor Padao
    /*! Chama o destrutor dos filhos do elemento
     */
    virtual ~NoAVL() {
        if (left)
            delete left;
        if (right)
            delete right;
        delete info;
        height = 0;
    }

    //! Retorna o valor da altura do nodo
    /*!
     * \return int altura do nodo
     */
    int getAltura() {
        return height;
    }

    //! Retorna como a arvore esta desbalanceada
    /*!
     * maior que 0 se desbalanco na esquerda
     * menor que 0 se desbalanco na direita
     * \param NoAVL<T>* root arvore para alterar altura
     * \return int desbalance do nodo
     */
    int getBalance(NoAVL<T>* root) {
        if (root)
            return getHeight(root->getLeft()) - getHeight(root->getRight());
        return 0;
    }

    //! Retorna o vetor elemento
    /*!
     * \return std::vector<NoAVL<T>*>
     */
    std::vector<NoAVL<T>* > getElementos() {
        return order;
    }

    //! Retorna o filho esquerdo do dado do nodo
    /*!
     * \return NoAVL<T>* filho esquerdo do nodo
     */
    NoAVL<T>* getEsquerda() {
        return left;
    }

    //! Retorna o filho direito do dado do nodo
    /*!
     * \return NoAVL<T>* filho direito do nodo
     */
    NoAVL<T>* getDireita() {
        return right;
    }

    //! Retorna o valor do dado do nodo
    /*!
     * \return T* dado do nodo
     */
    T* getDado() {
        return info;
    }

    //! Retorna Altura
    /*!
     * retorna -1 se for nodo folha
     * caso contrario a sua altura
     * \param NoAVL<T>* root arvore de onde pegar a altura
     * \return int altuda do nodo
     */
    int getHeight(NoAVL<T>* root) {
        if (root)
            return root->getAltura();
        return -1;
    }

    //! atualiza a altura do nodo
    /*!
     * atualiza a altura do nodo com o maximo da altura das suas subarvores mais 1.
     * \param NoAVL<T>* root nodo a atualizar
     * \return void
     */
    void setHeight(NoAVL<T>* root) {
        root->height =  std::max(getHeight(root->getLeft()), getHeight(root->getRight())) + 1;
    }

    //! Rotaciona a arvore a esquerda
    /*!
     * \param NoAVL<T>* root raiz a rotacionar
     * \return NoAVL<T>* a nova raiz da arvore
     */
    NoAVL<T>* rotateLeft(NoAVL<T>* root) {
        auto newRoot = root->getRight();
        root->right = newRoot->getLeft();
        newRoot->left = root;

        setHeight(root);
        setHeight(newRoot);

        return newRoot;
    }

    //! Rotaciona a arvore a direita
    /*!
     * \param NoAVL<T>* root raiz a rotacionar
     * \return NoAVL<T>* a nova raiz da arvore
     */
    NoAVL<T>* rotateRight(NoAVL<T>* root) {
        auto newRoot = root->getLeft();
        root->left = newRoot->getRight();
        newRoot->right = root;

        setHeight(root);
        setHeight(newRoot);

        return newRoot;
    }

    //! Rotacao dupla a direita
    /*!
     * \param NoAVL<T>* root raiz a rotacionar
     * \return NoAVL<T>* a nova raiz da arvore
     */
    NoAVL<T>* doubleRotateLeft(NoAVL<T>* root) {
        root->left = rotateLeft(root->getLeft());
        return rotateRight(root);
    }

    //! Rotacao dupla a direita
    /*!
     * \param NoAVL<T>* root raiz a rotacionar
     * \return NoAVL<T>* a nova raiz da arvore
     */
    NoAVL<T>* doubleRotateRight(NoAVL<T>* root) {
        root->right = rotateRight(root->getRight());
        return rotateLeft(root);
    }

    //! Insere dado na arvore
    /*!
     * Se a arvore estiver vazia o um nodo é criado com o dado e retornado.
     * Se o dado for maior que o dado da raiz da arvore,
            uma nova inserção é realizada na subarvore a direita
     * Se o dado for menor que o dado da raiz da arvore,
            uma nova inserção é realizada na subarvore a esquerda
     * Em seguida a arvore e balanceada
     * \param const T& info dado a ser inserido no nodo criado
     * \param NoAVL<T>* root arvore onde o dado sera inserida
     * \return NoAVL<T>* nodo criado
     */
    NoAVL<T>* inserir(const T& info, NoAVL<T>* root) {
        if (not root)
            return new NoAVL<T>{info};

        if (info < *(root->getInfo()))
            root->left = inserir(info, root->getLeft());
        else
            root->right = inserir(info, root->getRight());

        setHeight(root);

        int balance = getBalance(root);

        if (balance > 1 ) {
            if( info > *(root->left->getInfo()))
                root->left = rotateLeft(root->getLeft());
            return rotateRight(root);
        }

        if (balance < -1) {
            if ( info < *(root->right->getInfo()))
                root->right = rotateRight(root->getRight());
            return rotateLeft(root);
        }

        return root;
    }

    //! Remove o nodo com o dado da arvore
    /*!
     * Se a arvore estiver vazia retorna um ponteiro nulo.
     * Se o dado a ser removido é maior que o da raiz,
                retorna  uma nova remoção na subarvore a direita.
     * Se o dado a ser removido é menor que o da raiz,
            retorna  uma nova remoção na subarvore a esquerda.
     * Se o dado a ser removido é o dado da raiz, e
            existir uma arvore a direira e esquerda,
                o valor do nodo da raiz é preenchido com o menor nodo da
                subarvore a direita, e a direita desse nodo é configurada com
                a remoção do novo nodo na subarvore a direita, que é retornada.
     * Se existir uma arvore a esquerda ela é retornada.
     * Se existir uma arvore a direita ela é retornada.
     * Em seguida a arvore e rebalanceada
     * \param const T&info dado do nodo a ser removido
     * \param NoAVL<T>*root arvore onde o dado será removido
     * \return NoAVL<T>* nodo removido
     */
    NoAVL<T>* remover(NoAVL<T>* root, const T& info){
        if (not root)
            return root;

        if (info > *root->getInfo()) {
            root->right = remover(root->getRight(), info);
        } else if (info < *root->getInfo()) {
            root->left = remover(root->getLeft(), info);
        } else {
            if (root->right and root->left) {
                auto aux = minimo(root->getRight());
                *root->info = *aux->getInfo();
                root->right = remover(root->getRight(), *root->getInfo());
            } else {
                auto aux = root->getLeft() ? root->getLeft() : root->getRight();

                if (aux) {
                    return aux;
                } else {
                    delete root;
                    return nullptr;
                }
            }
        }

        if (not root)
            return root;

        setHeight(root);

        int balance = getBalance(root);

        if(balance > 1) {
            if(getBalance(root->getLeft()) < 0)
                root->left = rotateLeft(root->getLeft());
            return rotateRight(root);
        }

        if(balance < -1) {
            if(getBalance(root->getRight()) > 0)
                root->right = rotateRight(root->getRight());
            return rotateLeft(root);
        }

        return root;
    }

    //! Busca o nodo com menor valor de uma arvore
    /*!
     *  Se o nodo possuir um nodo a esquerda, o minimo deste é retornado.
     *  Caso contrario o nodo é retornado
     *  \param NoAVL<T>* nodo nodo inicial da busca
     *  \param NoAVL<T>* menor nodo
     */
    NoAVL<T>* minimo(NoAVL<T>* root) {
        if (root->getLeft())
            return minimo(root->getLeft());
        return root;
    }

    //! Busca o nodo com maior valor de uma arvore
    /*!
     *  Se o nodo possuir um nodo a direita, o maximo deste é retornado.
     *  Caso contrario o nodo é retornado
     *  \param NoAVL<T>* nodo nodo inicial da busca
     *  \param NoAVL<T>* maior nodo
     */
    NoAVL<T>* maximo(NoAVL<T>* root) {
        if (root->getRight())
            return maximo(root->getRight());
        return root;
    }

    //! Busca um nodo numa dada arvore
    /*!
     * Se a arvore estiver vazia da uma exeção,
     * Se o dado for menor que a raiz da arvore, uma nova busca é realizada
            na sua subarvore a esquerda.
     * Se o dado for maior que a raiz da arvore, uma nova busca é realizada
            na sua subarvore a direita.
     * Se o dado for encontrado ele é retornado.
     * \param const T& info dado a ser buscado na arvora
     * \param NoAVL<T>* arv arvore onde o dado sera buscado
     * \return T* dado do nodo
     */
    T* busca(const T& info, NoAVL<T>* root) {
        if (not root)
            throw "Key not found";
        if ( *(root->getInfo()) > info)
            return busca(info, root->getLeft());
        if (*(root->getInfo()) < info)
            return busca(info, root->getRight());
        return (root->getInfo());
    }

    //! Monta o vetor com os elementos em preOrdem
    /*!
     *  Se a raiz não for nula, ela é colocada no vetor,
     *      em seguida seu filho a esquerda é posto em preOrdem
     *      e depois o seu filho a direita e posto em preOrdem.
     *  \param NoBinario<T>* nodo nodo base
     */
    void preOrdem(NoAVL<T>* root) {
        if (root){
            order.push_back(root);
            preOrdem(root->getLeft());
            preOrdem(root->getRight());
        }
    }

    //! Monta o vetor com os elementos em emOrdem
    /*!
     *  Se a raiz não for nula, seu filho a esquerda é posto em emOrdem
            ela é colocada no vetor,
            em seguida seu filho a direita é posto em emOrdem.
     *  \param NoBinario<T>* nodo nodo base
     */
    void emOrdem(NoAVL<T>* root) {
        if (root) {
            emOrdem(root->getLeft());
            order.push_back(root);
            emOrdem(root->getRight());
        }
    }

    //! Monta o vetor com os elementos em posOrdem
    /*!
     *  Se a raiz não for nula, seu filho a esquerda é posto em posOrdem
            em seguida seu filho a direita é posto em posOrdem,
            e depois ela é colocada no vetor.
     *  \param NoBinario<T>* nodo nodo base
     */
    void posOrdem(NoAVL<T>* root) {
        if(root) {
            posOrdem(root->getLeft());
            posOrdem(root->getRight());
            order.push_back(root);
        }
    }
};

#endif /* NO_AVL_HPP */
