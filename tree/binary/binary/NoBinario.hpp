// Copyright 2016 Lucas Cavalcante de Sousa
//! Implementa��o estrutural da arvore binaria
/*! Estrutura que se comporta como uma arvore de objetos
 *  \author  Lucas Cavalcante de Sousa
 *  \since  1/4/16
 *  \version 1.0
 */#ifndef no_binario_hpp
#define NO_BINARIO_HPP

#include <cstdlib>
#include <vector>

template <typename T>
class NoBinario {
 protected:
    T dado;  //!< dado do nodo
    NoBinario<T>* esquerda{nullptr};  //!<  ponteiro para o filho da esquerda
    NoBinario<T>*direita{nullptr};  //!<  ponteiro para o filho da direita
    std::vector<NoBinario<T>* > elementos;  //!< Elementos acessados durante
                                            // o percurso realizado

 public:
    //! Construtor Pad�o
    /*!
     * \param const T& dado base para cira��o da arvore
     */
    NoBinario<T>(const T& other){
        dado = other;
    }
    //! Destrutor Padao
    /*! Chama o destrutor dos filhos do elemento
     */
    virtual ~NoBinario() {
        delete getEsquerda();
        delete getDireita();

    }
    //! Configura o filho esquerdo do nodo
    /*!
     * \param NoBinario<T>* novo filho esquerdo do nodo
     */
    void setEsquerda(NoBinario<T>* nodo) {
        esquerda = nodo;
    }
    //! Configura o filho direito do nodo
    /*!
     * \param NoBinario<T>* novo filho direito do nodo
     */
    void setDireita(NoBinario<T>* nodo) {
        direita = nodo;
    }
    //! Configura o valor do dado do nodo
    /*!
     * \param const T& novo dado do nodo
     */
    void setDado(const T& other) {
        dado = other;
    }
    //! Retorna o valor do dado do nodo
    /*!
     * \return T* dado do nodo
     */
    T* getDado() {
        return &dado;
    }
    //! Retorna o vetor elemento
    /*!
     * \return std::vector<NoBinario<T>*>
     */
    std::vector< NoBinario<T>* > getElementos() {
        return elementos;
    }
    //! Retorna o filho esquerdo do dado do nodo
    /*!
     * \return NoBinario<T>* filho esquerdo do nodo
     */
    NoBinario<T>* getEsquerda() {
        return esquerda;
    }
    //! Retorna o filho direito do dado do nodo
    /*!
     * \return NoBinario<T>* filho direito do nodo
     */
    NoBinario<T>* getDireita() {
        return direita;
    }
    //! Busca um nodo numa dada arvore
    /*!
     * Se a arvore estiver vazia da uma exe��o,
     * Se o dado for menor que a raiz da arvore, uma nova busca � realizada
            na sua subarvore a esquerda.
     * Se o dado for maior que a raiz da arvore, uma nova busca � realizada
            na sua subarvore a direita.
     * Se o dado for encontrado ele � retornado.
     * \param const T& dado dado a ser buscado na arvora
     * \param NoBinario<T>* arv arvore onde o dado sera buscado
     * \return T* dado do nodo
     */
    T* busca(const T& dado, NoBinario<T>* arv) {
        if (not arv)
            throw "Dado n�o encontrado";
        if ( *(arv->getDado()) > dado)
            return busca(dado, arv->getEsquerda());
        if (*(arv->getDado()) < dado)
            return busca(dado, arv->getDireita());
        return (arv->getDado());
    }
    //! Insere dado na arvore
    /*!
     * Se a arvore estiver vazia o um nodo � criado com o dado e retornado.
     * Se o dado for maior que o dado da raiz da arvore,
            e existir uma arvore a direita uma nova inser��o � realizada
                na subarvore a direita
            caso contrario cria-se o nodo com o dado a direita da raiz.
     * Se o dado for menor que o dado da raiz da arvore,
            e existir uma arvore a esquerda uma nova inser��o � realizada
                na subarvore a esquerda
            caso contrario cria-se o nodo com o dado a esquerda da raiz.
     * \param const T& dado dado a ser inserido no nodo criado
     * \param NoBinario<T>* arv arvore onde o dado sera inserida
     * \return NoBinario<T>* nodo criado
     */
    NoBinario<T>* inserir(const T& dado, NoBinario<T>* arv) {
        if (not arv)
            return new NoBinario<T>{dado};
        if (dado > *(arv->getDado())) {
            if (arv->getDireita())
                return inserir(dado, arv->getDireita());
            arv->setDireita(new NoBinario<T>{dado});
            return arv->getDireita();
        }
        if (dado <= *(arv->getDado())) {
            if (arv->esquerda)
                return inserir(dado, arv->getEsquerda());
            arv->setEsquerda(new NoBinario<T>{dado});
            return arv->getEsquerda();
        }
    }
    //! Remove o nodo com o dado da arvore
    /*!
     * Se a arvore estiver vazia retorna um ponteiro nulo.
     * Se o dado a ser removido � maior que o da raiz, e
            existir uma arvore a direita, retorna  uma nova remo��o
                na subarvore a direita.
            caso contrario ocorre uma exece��o.
     * Se o dado a ser removido � menor que o da raiz, e
            existir uma arvore a esquerda, retorna  uma nova remo��o
                na subarvore a esquerda.
            caso contrario ocorre uma exece��o.
     * Se o dado a ser removido � o dado da raiz, e
            existir uma arvore a direira e esquerda,
                o valor do nodo da raiz � preenchido com o menor nodo da
                subarvore a direita, e a direita desse nodo � configurada com
                a remo��o do novo nodo na subarvore a direita, que � retornada.
     * Se existir uma arvore a direita ela � retornada.
     * Se existir uma arvore a esquerda ela � retornada.
     * \param const T&dado dado do nodo a ser removido
     * \param NoBinario<T>*arv arvore onde o dado ser� removido
     * \return NoBinario<T>* nodo removido
     */
    NoBinario<T>* remover(NoBinario<T>* arv, const T& dado) {
        if (not arv) {
            return nullptr;
        } else if (dado > *(arv->getDado())) {
            if (arv->direita) {
                arv->setDireita(remover(arv->getDireita(), dado));
                return arv;
            }
            throw "Dado n�o encontrado";
        }else if (dado < *(arv->getDado())) {
            if (arv->esquerda) {
                arv->setEsquerda(remover(arv->getEsquerda(), dado));
                return arv;
            }
            throw "Dado n�o encontrado";
        } else {
            if (arv->direita and arv->esquerda) {
                auto aux = minimo(arv->getDireita());
                arv->setDado(*(aux->getDado()));
                arv->setDireita(remover(arv->getDireita(), *(arv->getDado())));
                return arv;
            } else if (arv->direita) {
                return arv->direita;
            }else if (arv->esquerda) {
                return arv->esquerda;
            }else {
                delete arv;
                return nullptr;
            }
        }
    }
    //! Busca o nodo com menor valor de uma arvore
    /*!
     *  Se o nodo possuir um nodo a esquerda, o minimo deste � retornado.
     *  Caso contrario o nodo � retornado
     *  \param NoBinario<T>* nodo nodo inicial da busca
     *  \param NoBinario<T>* menor nodo
     */
    NoBinario<T>* minimo(NoBinario<T>* nodo) {
        if (nodo->getEsquerda())
            return minimo(nodo->getEsquerda());
        return nodo;
    }
    //! Busca o nodo com maior valor de uma arvore
    /*!
     *  Se o nodo possuir um nodo a direita, o maximo deste � retornado.
     *  Caso contrario o nodo � retornado
     *  \param NoBinario<T>* nodo nodo inicial da busca
     *  \param NoBinario<T>* maior nodo
     */
    NoBinario<T>* maximo(NoBinario<T>* nodo) {
        if (nodo->getDireita())
            return maximo(nodo->getDireita());
        return nodo;
    }
    //! Monta o vetor com os elementos em preOrdem
    /*!
     *  Se a raiz n�o for nula, ela � colocada no vetor,
     *      em seguida seu filho a esquerda � posto em preOrdem
     *      e depois o seu filho a direita e posto em preOrdem.
     *  \param NoBinario<T>* nodo nodo base
     */
    void preOrdem(NoBinario<T>* nodo) {
        if (nodo){
            elementos.push_back(nodo);
            preOrdem(nodo->getEsquerda());
            preOrdem(nodo->getDireita());
        }
    }
    //! Monta o vetor com os elementos em emOrdem
    /*!
     *  Se a raiz n�o for nula, seu filho a esquerda � posto em emOrdem
            ela � colocada no vetor,
            em seguida seu filho a direita � posto em emOrdem.
     *  \param NoBinario<T>* nodo nodo base
     */
    void emOrdem(NoBinario<T>* nodo) {
        if (nodo) {
            preOrdem(nodo->getEsquerda());
            elementos.push_back(nodo);
            preOrdem(nodo->getDireita());
        }
    }
    //! Monta o vetor com os elementos em posOrdem
    /*!
     *  Se a raiz n�o for nula, seu filho a esquerda � posto em posOrdem
            em seguida seu filho a direita � posto em posOrdem,
            e depois ela � colocada no vetor.
     *  \param NoBinario<T>* nodo nodo base
     */
    void posOrdem(NoBinario<T>* nodo) {
        if(nodo) {
            posOrdem(nodo->getEsquerda());
            posOrdem(nodo->getDireita());
            elementos.push_back(nodo);
        } }
};

#endif /* NO_BINARIO_HPP */
