#ifndef STRUCTURES_TREE_H
#define STRUCTURES_TREE_H

#include <memory>

namespace structures {

template <typename T>
class Tree {
    struct Node {
        Node(T obj) :
            obj{std::move(obj)}
        {}

        std::unique_ptr<Node> left{nullptr};
        std::unique_ptr<Node> right{nullptr};
        T obj;

        auto &successor_ptr() {
            if (not right->left) {
                return right;
            }

            return right->min_ptr();
        }

        auto &predecessor_ptr() {
            if (not left->right) {
                return left;
            }

            return left->max_ptr();
        }

        auto &min_ptr() {
            if (not left->left) {
                return left;
            }

            return left->min_ptr();
        }

        auto &max_ptr() {
            if (not right->right) {
                return right;
            }

            return right->max_ptr();
        }

        std::unique_ptr<Node> &search_ptr(const T &other)
        {
            auto &ptr = search_place_ptr(other);

            if (not ptr) {
                throw std::out_of_range("Value not in tree");
            }

            return ptr;
        }

        std::unique_ptr<Node> &search_place_ptr(const T &other)
        {
            if (other < obj) {
                if (left) {
                    if (left->obj == other) {
                        return left;
                    }

                    return left->search_place_ptr(other);
                }

                return left;
            } else {
                if (right) {
                    if (right->obj == other) {
                        return right;
                    }

                    return right->search_place_ptr(other);
                }

                return right;
            }
        }

    };

public:
    void insert(T obj)
    {
        using std::move;
        using std::make_unique;

        auto &place = search_place_ptr(obj);

        if (not place) {
            place = make_unique<Node>(move(obj));
        }
    }

    const T &search(const T &obj)
    {
        return search_ptr(obj)->obj;
    }

    const T &min()
    {
        return min_ptr()->obj;
    }

    const T &max()
    {
        return max_ptr()->obj;
    }

    T remove(const T &obj)
    {
        using std::swap;

        auto &ptr = search_ptr(obj);

        auto aux = std::unique_ptr<Node>{nullptr};

        if (ptr->right) {
            auto &succ_ptr = ptr->successor_ptr();
            aux = move(succ_ptr);
            succ_ptr = move(aux->right);
            aux->right = move(ptr->right);
            aux->left = move(ptr->left);
        } else if (ptr->left) {
            auto &pred_ptr = ptr->predecessor_ptr();
            aux = move(pred_ptr);
            pred_ptr = move(aux->left);
            aux->right = move(ptr->right);
            aux->left = move(ptr->left);
        }

        swap(aux, ptr);

        return aux->obj;
    }

private:
    auto &search_ptr(const T &obj)
    {
        auto &ptr = search_place_ptr(obj);

        if (not ptr) {
            throw std::out_of_range("Tree is empty");
        }

        return ptr;
    }

    auto &search_place_ptr(const T &obj)
    {
        if (not root or root->obj == obj) {
            return root;
        }

        return root->search_place_ptr(obj);
    }

    auto &min_ptr() {
        if (not root) { throw std::out_of_range("Tree is empty"); }

        if (not root->left) {
            return root;
        }

        return root->min_ptr();
    }

    auto &max_ptr() {
        if (not root) { throw std::out_of_range("Tree is empty"); }

        if (not root->right) {
            return root;
        }

        return root->max_ptr();
    }

    std::unique_ptr<Node> root;
};

}

#endif
