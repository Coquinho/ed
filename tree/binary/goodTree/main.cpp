#include "Tree.h"

int main()
{
    using structures::Tree;
    auto tree = Tree<int>{};
    tree.insert(1);
    tree.insert(2);
    auto a = tree.min();
    auto b = tree.max();
    return a + b;
}
