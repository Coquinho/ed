#include "gtest/gtest.h"

#include "Tree.h"

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


struct TreeTest : public testing::Test {
    template <typename T>
    using Tree = structures::Tree<T>;

    Tree<int> ints;
};

TEST_F(TreeTest, empty_test)
{}

TEST_F(TreeTest, insert_one)
{
    ints.insert(10);
    ASSERT_TRUE(ints.search(10));
}

TEST_F(TreeTest, insert_many)
{
    {
        auto signal = 1;
        for (auto i = 1; i < 11; ++i) {
            ints.insert(i*signal);
            signal *= -1;
        }
    }

    {
        auto signal = 1;
        for (auto i = 1; i < 11; ++i) {
            auto value = i*signal;
            ASSERT_EQ(value, ints.search(value));
            signal *= -1;
        }
    }

    {
        auto signal = -1;
        for (auto i = 1; i < 11; ++i) {
            ASSERT_THROW(ints.search(i*signal), std::out_of_range);
            signal *= -1;
        }
    }
}

TEST_F(TreeTest, test_min_max) {
    for (auto i = 0; i < 10; ++i) {
        ints.insert(i);
    }

    ASSERT_EQ(0, ints.min());
    ASSERT_EQ(9, ints.max());
}

TEST_F(TreeTest, test_remove_linear) {
    for (auto i = 0; i < 10; ++i) {
        ints.insert(i);
    }

    ASSERT_EQ(5, ints.remove(5));
    ASSERT_THROW(ints.search(5), std::out_of_range);
}

TEST_F(TreeTest, test_remove_balanced) {
    ints.insert(5);
    ints.insert(2);
    ints.insert(8);
    ints.insert(1);
    ints.insert(4);
    ints.insert(7);
    ints.insert(9);
    ints.insert(3);
    ints.insert(6);

    ASSERT_EQ(5, ints.remove(5));
    ASSERT_THROW(ints.search(5), std::out_of_range);
    ASSERT_EQ(6, ints.remove(6));
    ASSERT_THROW(ints.search(6), std::out_of_range);
}

TEST_F(TreeTest, test_remove_balanced_2) {
    ints.insert(5);
    ints.insert(2);
    ints.insert(8);
    ints.insert(1);
    ints.insert(9);

    ASSERT_EQ(5, ints.remove(5));
    ASSERT_THROW(ints.search(5), std::out_of_range);
    ASSERT_EQ(8, ints.remove(8));
    ASSERT_THROW(ints.search(8), std::out_of_range);
    ASSERT_EQ(9, ints.remove(9));
    ASSERT_THROW(ints.search(9), std::out_of_range);
    ASSERT_EQ(2, ints.remove(2));
    ASSERT_THROW(ints.search(2), std::out_of_range);
    ASSERT_EQ(1, ints.remove(1));
    ASSERT_THROW(ints.search(1), std::out_of_range);
}
