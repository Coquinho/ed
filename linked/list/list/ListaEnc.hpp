// "Copyright 2016 Lucas Cavalcante de Sousa"
//! Implementação estrutural da lista encadiada
/*! Estrutura que se comporta como uma lista encadiada de objetos
 *  \author  Lucas Cavalcante de Sousa
 *  \since  19/4/16
 *  \version 1.0
 */
#include <cstdlib>
#include "Elemento.hpp"

template<typename T>
class ListaEnc {
 public:
    //! Construtor da lista encadiada
    ListaEnc() {}
    ~ListaEnc() {
        destroiLista();
    }
    // inicio
    // //! Adiciona dado ao inicio da lista
    /*! Se a lista não estiver cheia adiciona um elemento ao inicio
        caso contrario retorna um erro
     *  \param dado elemento a ser colocado no inicio da fila, tipo T
     *  \return void
     */
    void adicionaNoInicio(const T& dado) {
        adicionaNaPosicao(dado, 0);
    }
    //! Remove e retorna dado do inicio da lista
    /*! Se a lista não estiver vazia remove o elemento do incio
        caso contrario retorna um erro
     *  \return T
     */
    T retiraDoInicio() {
       return retiraDaPosicao(0);
    }
    //! Remove dado do inicio da lista
    /*! Se a lista não estiver vazia remove o elemento do incio
        caso contrario retorna um erro
     */
    void eliminaDoInicio() {
        retiraDaPosicao(0);
    }
    // posicao
    //! Adiciona dado na posicao desejada da lista
    /*! Se a posicao desejada for uma posicao valida , o dado é adicionado na posicao
        caso contrario retorna um erro
     *  \param dado elemento a ser colocado na lista, tipo T
     *  \param posicao posicao a ser colocado o dado, tipo int
     *  \return void
     */
    void adicionaNaPosicao(const T& dado, int pos) {
        if ((unsigned)pos > size)
            throw "Posicao invalida";
        if (pos == 0) {
            head = new Elemento<T>{dado, head};
        } else {
            auto prev = head;
            auto i = 0;
            auto it = head;
            for (; it != nullptr; it = it->getProximo()) {
                if (i == pos)
                    break;
                prev = it;
                ++i;
            }
            prev->setProximo(new Elemento<T>{dado, it});
        }
        ++size;
    }
    //! Verifica posicao de um elemento
    /*! Se a lista não estiver vazia verifica se o elemento desejado se encontra
        caso contrario retorna um erro
     *  \param dado elemento a ser verificado na lista, tipo T
     *  \return int , posicao
     */
    int posicao(const T& dado) const {
        if (listaVazia())
            throw "Lista Vazia";
        auto i = 0;
        for (auto it = head; it != nullptr; it = it->getProximo()) {
            if (it->getInfo() == dado)
                return i;
            ++i;
        }
        throw "Elemento Nao Encontrado";
    }
    //! Verifica a referencia de um dado
    /*! Se a lista não estiver vazia verifica se o dado desejado se encontra
        caso contrario retorna um erro
     *  \param dado a ser verificado na lista, tipo T
     *  \return T* , referencia do dado
     */
    T* posicaoMem(const T& dado) const {
        if (listaVazia())
            throw "Lista Vazia";
        for (auto it = head; it != nullptr; it = it->getProximo()) {
            if (it->getInfo == dado)
                return &(it->getInfo());
        }
        throw "Elemento não encontrado";
    }
    //! Verifica se o dado e contido na lista
    /*! Se a o dado estiver presente retorna true
        caso contrario retorna falso
     *  \param dado  a ser buscado na lista, tipo T
     *  \return bool
     */
    bool contem(const T& dado) {
        if (listaVazia())
            return false;
        for (auto it = head; it != nullptr; it = it->getProximo())
            if (it->getInfo() == dado)
                return true;
        return false;
    }
    //! Remove dado da posicao desejada
    /*! Se a lista não estiver vazia e a posicao desejada for valida,
        retira o elemento da posicao desejada
        caso contrario retorna um erro
     *  \param posicao posicao do elemento a ser retirado da lista, tipo int
     *  \return T
     */
    T retiraDaPosicao(int pos) {
        if (listaVazia())
            throw "Lista Vazia";
        if ((unsigned)pos >= size)
            throw "Posicao Invalida";
        auto value = head->getInfo();
        auto current = head;
        if (pos == 0) {
            delete current;
            head = head->getProximo();
        } else {
        // auto before = head;
            auto i = 0;
            for (auto it = head; it != nullptr; it = it->getProximo()) {
                if (i == pos) {
                    value = it->getInfo();
                    current->setProximo(it->getProximo());
                    delete it;
                    break;
                }
                ++i;
                // before = current;
                current = it;
            }
        }
        --size;
        return value;
    }
    // fim
    //! Adiciona dado ao final da lista
    /*!
     *  \param dado elemento a ser colocado ao fim da lista, tipo T
     *  \return void
     */
    void adiciona(const T& dado) {
        adicionaNaPosicao(dado, size);
    }
    //! Remove dado do final da lista
    /*! Se a lista não estiver vazia remove o dado ao fim
        caso contrario retorna um erro
     *  \return T, o ultimo dado
     */
    T retira() {
        return retiraDaPosicao(size-1);
    }
    // especifico
    //! Remove dado
    /*! Se a lista não estiver vazia e o dado desejado estiver na lista
        remove ele
        caso contrario retorna um erro
     *  \param dado a ser removido da lista, tipo T
     *  \return T
     */
    T retiraEspecifico(const T& dado) {
        if (listaVazia())
            throw "Lista Vazia";
        auto prev = head;
        for (auto it = head; it != nullptr; it = it->getProximo()) {
            auto value = it->getInfo();
            if (value == dado) {
                if (head->getInfo() == value)
                    head = head->getProximo();
                else
                    prev->setProximo(it->getProximo());
                delete it;
                --size;
                return value;
            }
            prev = it;
        }
        throw "Elemento Não Encontrado";
    }
    //! Adiciona dado em ordem
    /*! Adiciona um dado em ordem
     *  \param dado  a ser colocado na lista, tipo T
     *  \return void
     */
    void adicionaEmOrdem(const T& data) {
        if (head == nullptr || data <= head->getInfo()) {
            head = new Elemento<T>{data, head};
        } else {
            auto prev = head;
            auto it = head;
            for (; it != nullptr; it = it->getProximo()) {
                if (data <= it->getInfo()) {
                    prev->setProximo(new Elemento<T>{data, it});
                    break;
                }
                prev = it;
            }
            if (it == nullptr)
                prev->setProximo(new Elemento<T>{data, nullptr});
            ++size;
        }
    }
    //! Verifica se a lista esta vazia
    /*! Se a a lista esta vazia retorna true
        caso contrario retorna false
     *  \return bool
     */
    bool listaVazia() const {
        return (head == nullptr);
    }
    //! Verifica igualdade
    /*! Se a os dados forem iguais retorna true
        caso contrario retorna false
     *  \param dado1 elemento a ser verificado igualdade, tipo T
     *  \param dado2 elemento a ser verificado igualdade, tipo T
     *  \return bool
     */
    bool igual(T dado1, T dado2) {
        return (dado1 == dado2);
    }
    //! Verifica maior
    /*! Se a o dado1 for maior que o dado2 retorna true
        caso contrario retorna false
     *  \param dado1 elemento a ser verificado, tipo T
     *  \param dado2 elemento a ser verificado, tipo T
     *  \return bool
     */
    bool maior(T dado1, T dado2) {
        return (dado1 > dado2);
    }
    //! Verifica menor
    /*! Se a o dado1 for menor que o dado2 retorna true
        caso contrario retorna false
     *  \param dado1 elemento a ser verificado, tipo T
     *  \param dado2 elemento a ser verificado, tipo T
     *  \return bool
     */
    bool menor(T dado1, T dado2) {
        return (dado1 < dado2);
    }
    //! Destroi a lista
    /*! deleta toda a lista e seus dados
     *  \return void
     */
    void destroiLista() {
        auto prev = head;
        for (auto it = head; it != nullptr; it = it->getProximo()) {
            if (it != head) {
                delete prev;
                prev = it;
            }
        }
        size = 0;
        head = nullptr;
    }

 private:
    Elemento<T>* head = nullptr; //!< cabeça da lista, aponta para o primeiro elemento
                                //< se nao existir, aponta para nullptr

    std::size_t size = 0u; //!< tamanho da lista

};
