// "Copyright 2016 Lucas Cavalcante de Sousa"
#ifndef ELEMENTO_HPP
#define ELEMENTO_HPP

template<typename T>
class Elemento {
 private:
	T *info;
	Elemento<T>* _next;
	Elemento<T>* _before;

 public:
	Elemento(const T& info, Elemento<T>* next, Elemento<T>* before) :
            info(new T(info)), _next(next), _before(before){}

	~Elemento() {
		delete info;
	}

	Elemento<T>* getProximo() const {
		return _next;
	}

        Elemento<T>* getAnterior() const {
		return _before;
	}

	T getInfo() const {
		return *info;
	}

	void setProximo(Elemento<T>* next) {
		_next = next;
	}

        void setAnterior(Elemento<T>* before) {
		_before = before;
	}
};

#endif
