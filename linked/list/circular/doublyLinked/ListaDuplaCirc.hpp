// Copyright 2016 Lucas Cavalcante de Sousa
//! Implementação estrutural da lista duplamente encadeada circular
/*! Estrutura que se comporta como uma lista circular
 *  Onde o primeiro elemento e o head e o ultimo o tail,
 *  o seguinte do tail sempre e o head.
 *  Alem disso um elemento sempre aponta para o proximo, e
 *  para o antecesso.
 *  \author  Lucas Cavalcante de Sousa
 *  \since  03/5/16
 *  \version 1.0
 */
#include <cstdlib>
#include "Elemento.hpp"
template<typename T>
class ListaDuplaCirc {
 public:
    //! Construror padrao da lista duplamente encadeada circular
    ListaDuplaCirc() {}
    //! Destrutor da lista duplamente encadeada circular
    /*! Chama o destroi lista
     */
    ~ListaDuplaCirc() {
        destroiListaDuplo();
    }
    // Adiciona no inicio
    // //! Adiciona dado ao inicio da lista duplamente encadeada circular
    /*! Adiciona um elemento ao inicio
     *  \param dado elemento a ser colocado no inicio da fila, tipo T
     *  \return void
     */
    void adicionaNoInicioDuplo(const T& dado) {
        adicionaNaPosicaoDuplo(dado, 0);
    }
    //! Remove e retorna dado do inicio da lista duplamente encadeada circular
    /*! Se a lista não estiver vazia remove e retorna o elemento do incio
        caso contrario retorna um erro
     *  \return T
     */
    T retiraDoInicioDuplo() {
        return retiraDaPosicaoDuplo(0);
    }
    //! Remove dado do inicio da lista duplamente encadeada circular
    /*! Se a lista não estiver vazia remove o elemento do incio
        caso contrario retorna um erro
     *  \return void
     */
    void eliminaDoInicioDuplo() {
        retiraDaPosicaoDuplo(0);
    }
    // Adiciona na posicao
    //! Adiciona dado na posicao desejada da lista duplamente encadeada circular
    /*! Se a posicao desejada for uma posicao valida ,
     *  o dado é adicionado na posicao
     *  caso contrario retorna um erro
     *  \param dado elemento a ser colocado na lista, tipo T
     *  \param posicao posicao a ser colocado o dado, tipo int
     *  \return void
     */
    void adicionaNaPosicaoDuplo(const T& dado, int pos) {
        if ((unsigned)pos > size)
            throw "Posocao Invalida";
        if (pos == 0 || (unsigned)pos == size) {
            auto newOffset = new Elemento<T>{dado, head, tail};
            if (head == nullptr) {
                newOffset->setProximo(newOffset);
                newOffset->setAnterior(newOffset);
                tail = newOffset;
                head = newOffset;
            } else if (pos == 0) {
                head->setAnterior(newOffset);
                head = newOffset;
                tail->setProximo(head);
            } else {
                tail->setProximo(newOffset);
                tail = newOffset;
                head->setAnterior(tail);
            }
        } else {
            auto prev = head;
            auto it = head->getProximo();
            auto i = 1;
            while (it != head) {
                if (i == pos) {
                    auto current = new Elemento<T>{dado, it, prev};
                    prev->setProximo(current);
                    it->setAnterior(current);
                    break;
                }
                ++i;
                it = it->getProximo();
            }
        }
        ++size;
    }
    //! Verifica posicao de um elemento na lista duplamente encadeada  circular
    /*! Se a lista não estiver vazia verifica se o elemento desejado se encontra
        caso contrario retorna um erro
     *  \param dado elemento a ser verificado na lista, tipo T
     *  \return int , posicao
     */
    int posicaoDuplo(const T& dado) const {
        if (listaVazia())
            throw "Lista Vazia";
        if (dado == tail->getInfo())
            return (size-1);
        auto i = 0;
        auto it = head;
        while (it != tail) {
            if (it->getInfo() == dado)
                return i;
            ++i;
            it = it->getProximo();
        }
        throw "Dado Nao Encontrado";
    }
    //! Verifica a referencia de um dado na lista duplamente encadeada circular
    /*! Se a lista não estiver vazia verifica se o dado desejado se encontra
        caso contrario retorna um erro
     *  \param dado a ser verificado na lista, tipo T
     *  \return T* , referencia do dado
     */
    T* posicaoMemDuplo(const T& dado) const {
        if (listaVazia())
            throw "Lista Vazia";
        if (dado = tail->getInfo())
            return &(tail->getInfo());
        auto it = head;
        while (it != tail) {
            if (dado == it->getInfo())
                return &(it->getInfo());
            it = it->getProximo();
        }
        throw "Dado Nao Encontrado";
    }
    //! Verifica se o dado e contido na lista duplamente encadeada circular
    /*! Se a o dado estiver presente retorna true
        caso contrario retorna falso
     *  \param dado  a ser buscado na lista, tipo T
     *  \return bool
     */
    bool contemDuplo(const T& dado) {
        if (listaVazia())
            return false;
        if (dado == tail->getInfo())
            return true;
        auto it = head;
        while (it != tail) {
            if (dado == it->getInfo())
                return true;
            it = it->getProximo();
        }
        return false;
    }
    //! Remove dado da posicao desejada
    /*! Se a lista não estiver vazia e a posicao desejada for valida,
        retira o elemento da posicao desejada
        caso contrario retorna um erro
     *  \param posicao posicao do elemento a ser retirado da lista, tipo int
     *  \return T
     */
    T retiraDaPosicaoDuplo(int pos) {
        if (listaVazia())
            throw "ListaVazia";
        if ((unsigned)pos > size)
            throw "Posicao Invalida";
        T value;
        if (size == 1) {
            value = head->getInfo();
            tail = nullptr;
            delete head;
            head = nullptr;
        } else if (pos == 0) {
            value = head->getInfo();
            tail->setProximo(head->getProximo());
            delete head;
            head = tail->getProximo();
            head->setAnterior(tail);
        } else {
            auto prev = head;
            auto it = head->getProximo();
            auto i = 1;
            while (prev != tail) {
                if (i == pos) {
                    value = it->getInfo();
                    if (it == tail)
                        tail = prev;
                    prev->setProximo(it->getProximo());
                    it->getProximo()->setAnterior(prev);
                    delete it;
                    break;
                }
                ++i;
                prev = it;
                it = it->getProximo();
            }
        }
        --size;
        return value;
    }
    // Adiciona no fim da lista duplamente encadeada circular
    //! Adiciona dado ao final da lista
    /*!
     *  \param dado elemento a ser colocado ao fim da lista, tipo T
     *  \return void
     */
    void adicionaDuplo(const T& dado) {
        adicionaNaPosicaoDuplo(dado, size);
    }
    //! Remove dado do final da lista duplamente encadeada circular
    /*! Se a lista não estiver vazia remove o dado ao fim
        caso contrario retorna um erro
     *  \return T, o ultimo dado
     */
    T retiraDuplo() {
        return retiraDaPosicaoDuplo(size-1);
    }
    // Retira Especifico
    //! Remove dado desejado
    /*! Se a lista não estiver vazia e o dado desejado estiver na lista
        remove ele
        caso contrario retorna um erro
     *  \param dado a ser removido da lista, tipo T
     *  \return T
     */
    T retiraEspecificoDuplo(const T& dado) {
        return retiraDaPosicaoDuplo(posicaoDuplo(dado));
    }
    // Verifica o dado da posicao desejada
    //! Verifica o que ha na posicao desejada da lista duplamente encadeada circular
    /*! Se a lista não estiver vazia e existir tal posicao na lista
        retorna o dado
        caso contrario retorna um erro
     *  \param pos, posicao a ser verificada, int
     *  \return T, o dado na posicao desejada
     */
    T mostra(int pos) {
        if (listaVazia())
            throw "Lista Vazia";
        if ((unsigned)pos == size-1)
            return tail->getInfo();
        auto it = head;
        auto i = 0;
        while (it != tail) {
            if (pos == i)
                return it->getInfo();
            it = it->getProximo();
            ++i;
        }
        throw "Dado Nao Encontrado";
    }
    //! Adiciona dado em ordem
    /*! Adiciona um dado em ordem
     *  \param dado  a ser colocado na lista, tipo T
     *  \return void
     */
    void adicionaEmOrdem(const T& data) {
        if (size == 0 || data > tail->getInfo() || data <= head->getInfo() ) {
            auto newOffset = new Elemento<T>{data, head, tail};
            if (head == nullptr) {
                newOffset->setProximo(newOffset);
                newOffset->setAnterior(newOffset);
                tail = newOffset;
                head = newOffset;
            } else if (data <= head->getInfo()) {
                head->setAnterior(newOffset);
                head = newOffset;
                tail->setProximo(head);
            } else {
                tail->setProximo(newOffset);
                tail = newOffset;
                head->setAnterior(tail);
            }
        } else {
            auto it = head->getProximo();
            auto prev = head;
            while (it != head) {
                if (data < it->getInfo()) {
                    auto current = new Elemento<T>{data, it, prev};
                    prev->setProximo(current);
                    it->setAnterior(current);
                }
                prev = it;
                it = it->getProximo();
            }
        }
        ++size;
    }
    // Verifica a posicao do ultimo dado
    //! Verifica a posicao do ultimo dado da lista duplamente encadeada circular
    /*! Se a lista não estiver vazia, retorna posicao do ultimo dado
        caso contrario retorna um erro
     *  \return int, posicao do ultimo dado
     */
    int verUltimo() {
        if (listaVazia())
            throw "Lista Vazia";
        return (size-1);
    }
    //! Verifica se a lista esta vazia
    /*! Se a a lista esta vazia retorna true
        caso contrario retorna false
     *  \return bool
     */
    bool listaVazia() const {
        return (size == 0);
    }
    //! Verifica igualdade
    /*! Se a os dados forem iguais retorna true
        caso contrario retorna false
     *  \param dado1 elemento a ser verificado igualdade, tipo T
     *  \param dado2 elemento a ser verificado igualdade, tipo T
     *  \return bool
     */
    bool igual(T dado1, T dado2) {
        return (dado1 == dado2);
    }
    //! Verifica maior
    /*! Se a o dado1 for maior que o dado2 retorna true
        caso contrario retorna false
     *  \param dado1 elemento a ser verificado, tipo T
     *  \param dado2 elemento a ser verificado, tipo T
     *  \return bool
     */
    bool maior(T dado1, T dado2) {
        return (dado1 > dado2);
    }
    //! Verifica menor
    /*! Se a o dado1 for menor que o dado2 retorna true
        caso contrario retorna false
     *  \param dado1 elemento a ser verificado, tipo T
     *  \param dado2 elemento a ser verificado, tipo T
     *  \return bool
     */
    bool menor(T dado1, T dado2) {
        return (dado1 < dado2);
    }
    //! Destroi a listaDuplamente  Encadeada Circular
    /*! Deleta toda a lista e seus dados
     *  \return void
     */
    void destroiListaDuplo() {
        while (head != nullptr)
            eliminaDoInicioDuplo();
        tail = nullptr;
        head = nullptr;
        size = 0u;
    }

 private:
    Elemento<T>*head = nullptr;  //!< cabeça da lista,
                                //< aponta para o primeiro elemento,
                               //<  se nao existir, aponta para nullptr
    Elemento<T>*tail = nullptr;  //!< fim da lista,
                                //< aponta para o ultimo elemento
                               //< se nao existir, aponta para nullptr
    std::size_t size = 0u;  //!< tamanho da lista
};
