// Copyright 2016 Lucas Cavalcante de Sousa
//! Implementação estrutural da lista circular
/*! Estrutura que se comporta como uma lista circular
 *  Onde o primeiro elemento e o head e o ultimo o tail,
 *  o seguinte do tail sempre e o head.
 *  Alem disso um elemento sempre aponta para o proximo.
 *  \author  Lucas Cavalcante de Sousa
 *  \since  18/4/16
 *  \version 1.0
 */
#include <cstdlib>
#include "Elemento.hpp"
template<typename T>
class ListaCirc {
 public:
    //! Construror padrao da lista circular
    ListaCirc() {}
    //! Destrutor da lista circular
    /*! Chama o destroi lista
     */
    ~ListaCirc() {
        destroiLista();
    }
    // Adiciona no inicio
    // //! Adiciona dado ao inicio da lista circular
    /*! Se a lista não estiver cheia adiciona um elemento ao inicio
        caso contrario retorna um erro
     *  \param dado elemento a ser colocado no inicio da fila, tipo T
     *  \return void
     */
    void adicionaNoInicio(const T& dado) {
        adicionaNaPosicao(dado, 0);
    }
    //! Remove e retorna dado do inicio da lista circular
    /*! Se a lista não estiver vazia remove o elemento do incio
        caso contrario retorna um erro
     *  \return T
     */
    T retiraDoInicio() {
        return retiraDaPosicao(0);
    }
    //! Remove dado do inicio da lista circular
    /*! Se a lista não estiver vazia remove o elemento do incio
        caso contrario retorna um erro
     *  \return void
     */
    void eliminaDoInicio() {
        retiraDaPosicao(0);
    }
    // Adiciona na posicao
    //! Adiciona dado na posicao desejada da lista circular
    /*! Se a posicao desejada for uma posicao valida , o dado é adicionado na posicao
        caso contrario retorna um erro
     *  \param dado elemento a ser colocado na lista, tipo T
     *  \param posicao posicao a ser colocado o dado, tipo int
     *  \return void
     */
    void adicionaNaPosicao(const T& dado, int pos) {
        if ((unsigned)pos > size)
            throw "Posocao Invalida";
        if (pos == 0 || (unsigned)pos == size) {
            auto newOffset = new Elemento<T>{dado, head};
            if (head == nullptr) {
                newOffset->setProximo(newOffset);
                tail = newOffset;
                head = newOffset;
            } else if (pos == 0) {
                head = newOffset;
                tail->setProximo(head);
            } else {
                tail->setProximo(newOffset);
                tail = newOffset;
            }
        } else {
            auto it = head;
            auto i = 1;
            while (it != tail) {
                if (i == pos) {
                    auto current = new Elemento<T>{dado, it->getProximo()};
                    it->setProximo(current);
                    break;
                }
                ++i;
                it = it->getProximo();
            }
        }
        ++size;
    }
    //! Verifica posicao de um elemento na lista circular
    /*! Se a lista não estiver vazia verifica se o elemento desejado se encontra
        caso contrario retorna um erro
     *  \param dado elemento a ser verificado na lista, tipo T
     *  \return int , posicao
     */
    int posicao(const T& dado) const {
        if (listaVazia())
            throw "Lista Vazia";
        if (dado == tail->getInfo())
            return (size-1);
        auto i = 0;
        auto it = head;
        while (it != tail) {
            if (it->getInfo() == dado)
                return i;
            ++i;
            it = it->getProximo();
        }
        throw "Dado Nao Encontrado";
    }
    //! Verifica a referencia de um dado na lista circular
    /*! Se a lista não estiver vazia verifica se o dado desejado se encontra
        caso contrario retorna um erro
     *  \param dado a ser verificado na lista, tipo T
     *  \return T* , referencia do dado
     */
    T* posicaoMem(const T& dado) const {
        if (listaVazia())
            throw "Lista Vazia";
        if (dado = tail->getInfo())
            return &(tail->getInfo());
        auto it = head;
        while (it != tail) {
            if (dado == it->getInfo())
                return &(it->getInfo());
            it = it->getProximo();
        }
        throw "Dado Nao Encontrado";
    }
    //! Verifica se o dado e contido na lista circular
    /*! Se a o dado estiver presente retorna true
        caso contrario retorna falso
     *  \param dado  a ser buscado na lista, tipo T
     *  \return bool
     */
    bool contem(const T& dado) {
        if (listaVazia())
            return false;
        if (dado == tail->getInfo())
            return true;
        auto it = head;
        while (it != tail) {
            if (dado == it->getInfo())
                return true;
            it = it->getProximo();
        }
        return false;
    }
    //! Remove dado da posicao desejada
    /*! Se a lista não estiver vazia e a posicao desejada for valida,
        retira o elemento da posicao desejada
        caso contrario retorna um erro
     *  \param posicao posicao do elemento a ser retirado da lista, tipo int
     *  \return T
     */
    T retiraDaPosicao(int pos) {
        if (listaVazia())
            throw "ListaVazia";
        if ((unsigned)pos > size)
            throw "Posicao Invalida";
        T value;
        if (pos == 0) {
            value = head->getInfo();
            if (size == 1)
                tail = nullptr;
            else
                tail->setProximo(head->getProximo());
            delete head;
            if (size == 1)
                head = nullptr;
            else
                head = tail->getProximo();
        } else {
            auto prev = head;
            auto it = head->getProximo();
            auto i = 1;
            while (it != head) {
                if (i == pos) {
                    value = it->getInfo();
                    if (it == tail)
                        tail = prev;
                    prev->setProximo(it->getProximo());
                    delete it;
                    break;
                }
                ++i;
                prev = it;
                it = it->getProximo();
            }
        }
        --size;
        return value;
    }
    // Adiciona no fim da lista circular
    //! Adiciona dado ao final da lista
    /*!
     *  \param dado elemento a ser colocado ao fim da lista, tipo T
     *  \return void
     */
    void adiciona(const T& dado) {
        adicionaNaPosicao(dado, size);
    }
    //! Remove dado do final da lista circular
    /*! Se a lista não estiver vazia remove o dado ao fim
        caso contrario retorna um erro
     *  \return T, o ultimo dado
     */
    T retira() {
        return retiraDaPosicao(size-1);
    }
    // Retira Especifico
    //! Remove dado
    /*! Se a lista não estiver vazia e o dado desejado estiver na lista
        remove ele
        caso contrario retorna um erro
     *  \param dado a ser removido da lista, tipo T
     *  \return T
     */
    T retiraEspecifico(const T& dado) {
        return retiraDaPosicao(posicao(dado));
    }
    // Verifica o dado da posicao desejada
    //! Verifica o que ha na posicao desejada da lista circular
    /*! Se a lista não estiver vazia e existir tal posicao na lista
        retorna o dado
        caso contrario retorna um erro
     *  \param pos, posicao a ser verificada, int
     *  \return T, o dado na posicao desejada
     */
    T mostra(int pos) {
        if (listaVazia())
            throw "Lista Vazia";
        if ((unsigned)pos == size-1)
            return tail->getInfo();
        auto it = head;
        auto i = 0;
        while (it != tail) {
            if (pos == i)
                return it->getInfo();
            it = it->getProximo();
            ++i;
        }
        throw "Dado Nao Encontrado";
    }
    //! Adiciona dado em ordem
    /*! Adiciona um dado em ordem
     *  \param dado  a ser colocado na lista, tipo T
     *  \return void
     */
    void adicionaEmOrdem(const T& data) {
        if (listaVazia())
            adicionaNoInicio(data);
        if (data > tail->getInfo() || data < head->getInfo()) {
            auto newOffset = new Elemento<T>{data, head};
            if (head == nullptr) {
                newOffset->setProximo(newOffset);
                tail = newOffset;
            } else if (data < head->getInfo()) {
                head = newOffset;
                tail->setProximo(head);
            } else {
                tail->setProximo(newOffset);
                tail = newOffset;
            }
        } else {
            auto it = head->getProximo();
            auto prev = head;
            while (it != head) {
                if (data < it->getInfo()) {
                    auto current = new Elemento<T>{data, it};
                    prev->setProximo(current);
                }
                prev = it;
                it = it->getProximo();
            }
        }
        ++size;
    }
    // Verifica a posicao do ultimo dado
    //! Verifica a posicao do ultimo dado da lista circular
    /*! Se a lista não estiver vazia, retorna posicao do ultimo dado
        caso contrario retorna um erro
     *  \return int, posicao do ultimo dado
     */
    int verUltimo() {
        if (listaVazia())
            throw "Lista Vazai";
        return (size-1);
    }
    //! Verifica se a lista esta vazia
    /*! Se a a lista esta vazia retorna true
        caso contrario retorna false
     *  \return bool
     */
    bool listaVazia() const {
        return (size == 0);
    }
    //! Verifica igualdade
    /*! Se a os dados forem iguais retorna true
        caso contrario retorna false
     *  \param dado1 elemento a ser verificado igualdade, tipo T
     *  \param dado2 elemento a ser verificado igualdade, tipo T
     *  \return bool
     */
    bool igual(T dado1, T dado2) {
        return (dado1 == dado2);
    }
    //! Verifica maior
    /*! Se a o dado1 for maior que o dado2 retorna true
        caso contrario retorna false
     *  \param dado1 elemento a ser verificado, tipo T
     *  \param dado2 elemento a ser verificado, tipo T
     *  \return bool
     */
    bool maior(T dado1, T dado2) {
        return (dado1 > dado2);
    }
    //! Verifica menor
    /*! Se a o dado1 for menor que o dado2 retorna true
        caso contrario retorna false
     *  \param dado1 elemento a ser verificado, tipo T
     *  \param dado2 elemento a ser verificado, tipo T
     *  \return bool
     */
    bool menor(T dado1, T dado2) {
        return (dado1 < dado2);
    }
    //! Destroi a lista Encadeada Circular
    /*! Deleta toda a lista e seus dados
     *  \return void
     */
    void destroiLista() {
        while (head != nullptr) {
            tail->setProximo(head->getProximo());
            if (tail == head) {
                tail->setProximo(nullptr);
                tail = nullptr;
            }
            delete head;
            head = tail;
        }
        tail = nullptr;
        head = nullptr;
        size = 0u;
    }

 private:
    Elemento<T>*head = nullptr;  //!< cabeça da lista,
                                //< aponta para o primeiro elemento,
                               //<  se nao existir, aponta para nullptr
    Elemento<T>*tail = nullptr;  //!< fim da lista,
                                //< aponta para o ultimo elemento
                               //< se nao existir, aponta para nullptr
    std::size_t size = 0u;  //!< tamanho da lista
};
