// "Copyright 2016 Lucas Cavalcante de Sousa"
//! Implementação estrutural da fila encadiada
/*! Estrutura que se comporta como uma fila encadiada de objetos
 *  \author  Lucas Cavalcante de Sousa
 *  \since  22/4/16
 *  \version 1.0
 */
#include <cstdlib>
#include "Elemento.hpp"
template <typename T>
class FilaEnc{
 public:
    //! Construror da fila encadiada padrao
    FilaEnc<T>() {}
    //! destrutor da fila encadiada
    /*! Chama o destroi lista
     */
    ~FilaEnc() {
        limparFila();
    }
    //! inclui dado à fila encadiada
    /*! Adiciona um elemento ao fim e incremeta em 1 o tamanho da fila encadiada
        se este for o primeiro elemento a ser incluido, head = tail
     *  \param dado dado a ser adicionado à fila encadiada
     *  \return void
     */
    void inclui(const T& dado) {
        auto last = new Elemento<T>{dado, nullptr};
        if (tail != nullptr)
            tail->setProximo(last);
        tail = last;
        if (head == nullptr)
            head = tail;
        ++size;
    }
    //! retira dado da fila encadiada
    /*! Se a pilha estiver vaizia retorna um erro,
        caso contrario r etira e retorna o primeiro elemento da fila encadiada
        caso contrario retorna um erro
     *  \return um T
     */
    T retira() {
        if (filaVazia())
            throw "Lista Vazia";
        auto prev = head;
        auto value = head->getInfo();
        head = head->getProximo();
        delete prev;
        --size;
        return value;
    }
    //! retorna o ùltimo elemento da fila
    /*! Se a fila não estiver vazia, retorna o último elemento da fila
        caso contrario retorna um erro
     *  \return um T
     */
    T ultimo() {
        if (filaVazia())
            throw "Lista Vazia";
        return (tail->getInfo());
    }
    //! Retorna o primeiro elemento  da fila encadiada
    /*! se a fila não estiver vazia, retorna o indice o primeiro elemento
        caso contrario retorna um erro
     *  \return um inteiro
     */
    T primeiro() {
        if (filaVazia())
            throw "Lista Vazia";
        return (head->getInfo());
    }
    //! Verifica se a fila encadiada está vazia
    /*! se a fila estiver vazia, retorna true
        caso contrario retorna false
     *  \return boolean
     */
    bool filaVazia() {
        return (head == nullptr);
    }
    //! Destroi a fila encadiada
    /*! Deleta toda a fila e seus dados,
        faz os ponteiros para comeco e fim apontarem para nullptr
        e size = 0
     *  \return void
     */
    void limparFila() {
        for (auto it = head; it != tail; it = head) {
            head = it->getProximo();
            delete it;
        }
        if (tail == nullptr)
            delete tail;
        head = nullptr;
        tail = nullptr;
        size = 0u;
    }

 private:
    Elemento<T>*head = nullptr;  //!< cabeça da lista,
                                //< aponta para o primeiro elemento,
                               //<  se nao existir, aponta para nullptr
    Elemento<T>*tail = nullptr;  //!< fim da lista,
                                //< aponta para o ultimo elemento
                                //< se nao existir, aponta para nullptr
    std::size_t size = 0u;  //!< tamanho da lista
};

