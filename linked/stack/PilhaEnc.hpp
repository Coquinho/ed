template<typename T>
class PilhaEnc:private ListaEnc<T>{
 public:
    PilhaEnc()
    ~PilhaEnc(){
        ListaEnc<T>::destroiLista();
    }
    void empilha(const T& dado) {
        ListaEnc<T>::adicionaDoInicio();
    }
    T desempilha() {
        ListaEnc<T>::retiraDoInicio();
    }
    T topo() {
        return ListaEnc<T>::getHeadInfo();
    }
    void limparPilha() {
        ListaEnc<T>::destroiLista();
    }
    bool PilhaVazia() {
        return ListaEnc<T>::listaVazia();
    }
};
