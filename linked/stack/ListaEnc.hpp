// "Copyright 2016 Lucas Cavalcante de Sousa"
#include <cstdlib>
#include <iostream>
#include "Elemento.hpp"

template<typename T>
class ListaEnc {
 public:
    std::size_t getSize() {
        return size;
    }
    void setSize(_size) {
        size = _size;
    }
    T getHeadInfo(){
        if(listaVazia())
            return head->getInfo();
        throw "Lsta Vazia";
    }
    void setHead(_head) {
        head = _head;
    }
    ListaEnc() {}
    ~ListaEnc() {
        destroiLista();
    }
    // inicio
    void adicionaNoInicio(const T& dado) {
        adicionaNaPosicao(dado, 0);
    }
    T retiraDoInicio() {
       return retiraDaPosicao(0);
    }
    void eliminaDoInicio() {
        retiraDaPosicao(0);
    }
    // posicao
    void adicionaNaPosicao(const T& dado, int pos) {
        if ((unsigned)pos > size)
            throw "Posicao invalida";
        if (pos == 0) {
            head = new Elemento<T>{dado, head};
        } else {
            auto prev = head;
            auto i = 0;
            auto it = head;
            for (; it != nullptr; it = it->getProximo()) {
                if (i == pos)
                    break;
                prev = it;
                ++i;
        }
            prev->setProximo(new Elemento<T>{dado, it});
        }
        ++size;
    }
    int posicao(const T& dado) const {
        if (listaVazia())
            throw "Lista Vazia";
        auto i = 0;
        for (auto it = head; it != nullptr; it = it->getProximo()) {
            if (it->getInfo() == dado)
                return i;
            ++i;
        }
        throw "Elemento Nao Encontrado";
    }
    T* posicaoMem(const T& dado) const {
        if (listaVazia())
            throw "Lista Vazia";
        for (auto it = head; it != nullptr; it = it->getProximo()) {
            if (it->getInfo == dado)
                return &(it->getInfo());
        }
    }
    bool contem(const T& dado) {
        if (listaVazia())
            return false;
        for (auto it = head; it != nullptr; it = it->getProximo())
            if (it->getInfo() == dado)
                return true;
        return false;
    }
    T retiraDaPosicao(int pos) {
        if (listaVazia())
            throw "Lista Vazia";
        if ((unsigned)pos >= size)
            throw "Posicao Invalida";
        auto value = head->getInfo();
        auto current = head;
        if (pos == 0) {
            delete current;
            head = head->getProximo();
        } else {
        // auto before = head;
            auto i = 0;
            for (auto it = head; it != nullptr; it = it->getProximo()) {
                if (i == pos) {
                    value = it->getInfo();
                    current->setProximo(it->getProximo());
                    delete it;
                    break;
                }
                ++i;
                // before = current;
                current = it;
            }
        }
        --size;
        return value;
    }
    // fim
    void adiciona(const T& dado) {
        adicionaNaPosicao(dado, size);
    }
    T retira() {
        return retiraDaPosicao(size-1);
    }
    // especifico
    T retiraEspecifico(const T& dado) {
        if (listaVazia())
            throw "Lista Vazia";
        auto prev = head;
        for (auto it = head; it != nullptr; it = it->getProximo()) {
            auto value = it->getInfo();
            if (value == dado) {
                if (head->getInfo() == value)
                    head = head->getProximo();
                else
                    prev->setProximo(it->getProximo());
                delete it;
                --size;
                return value;
            }
            prev = it;
        }
        throw "Elemento Não Encontrado";
    }
    void adicionaEmOrdem(const T& data) {
        if (head == nullptr || data <= head->getInfo()) {
            head = new Elemento<T>{data, head};
        } else {
            auto prev = head;
            auto it = head;
            for (; it != nullptr; it = it->getProximo()) {
                if (data <= it->getInfo()) {
                    prev->setProximo(new Elemento<T>{data, it});
                    break;
                }
                prev = it;
            }
            if (it == nullptr)
                prev->setProximo(new Elemento<T>{data, nullptr});
        }
        ++size;
    }
    bool listaVazia() const {
        return (head == nullptr);
    }
    bool igual(T dado1, T dado2) {
        return (dado1 == dado2);
    }
    bool maior(T dado1, T dado2) {
        return (dado1 > dado2);
    }
    bool menor(T dado1, T dado2) {
        return (dado1 < dado2);
    }
    void destroiLista() {
        auto prev = head;
        for (auto it = head; it != nullptr; it = it->getProximo()) {
            if (it != head) {
                delete prev;
                prev = it;
            }
        }
        size = 0;
        head = nullptr;
    }

 private:
    Elemento<T>* head = nullptr;
    std::size_t size = 0u;
};
